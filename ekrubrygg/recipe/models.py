from math import fsum

from django.db import models
from django.conf import settings

from ordered_model.models import OrderedModel
from tinymce import HTMLField

class Recipe(models.Model):
    name = models.CharField(max_length=99)
    brew_category = models.ForeignKey('BrewCategory', null=True, on_delete=models.SET_NULL)
    public = models.BooleanField(default=False)
    original_gravity = models.FloatField(blank=True, null=True)
    final_gravity = models.FloatField(blank=True, null=True)
    ph = models.FloatField(blank=True, null=True)
    ingredients = models.ManyToManyField('Ingredient', through='IngredientInRecipe', name='ingredients', related_name='recipe')
    pub_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['-pub_date']

    def __str__(self):
        return self.name

    def calculate_abv(self):
        if self.original_gravity and self.final_gravity:
            return '{0:.2f}'.format((self.original_gravity - self.final_gravity) * self.brew_category.abv_factor)
        else:
            return 'Add both OG and FG to get ABV.'


class BrewCategory(models.Model):
    name = models.CharField(max_length=99)
    category = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)
    abv_factor = models.FloatField()

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=99)
    description = models.TextField(blank=True, null=True)
    category = models.ForeignKey('IngredientCategory', on_delete=models.CASCADE)
    manufacturer = models.ForeignKey('Manufacturer', related_name='ingredient', on_delete=models.CASCADE)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = ['name', 'manufacturer', 'created_by']

    def __str__(self):
        return '%s by %s' % (self.name, self.manufacturer.name)


class IngredientCategory(models.Model):
    name = models.CharField(max_length=99)
    category = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class IngredientInRecipe(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, related_name='in_recipe', on_delete=models.CASCADE)
    amount = models.FloatField()
    amount_unit = models.ForeignKey('AmountUnit', null=True, on_delete=models.SET_NULL)
    step = models.ManyToManyField('Step', through='IngredientInStep', name='steps', related_name='ingredient_in_recipe')

    def __str__(self):
        return '%s by %s' % (self.ingredient.name, self.ingredient.manufacturer.name)

    def amount_left(self):
        return self.amount - fsum([x.amount for x in self.ingredientinstep_set.all()])

    def get_amount(self):
        if self.amount == int(self.amount):
            return int(self.amount)
        else:
            return self.amount


class IngredientInStep(models.Model):
    ingredient = models.ForeignKey(IngredientInRecipe, on_delete=models.CASCADE)
    step = models.ForeignKey('Step', on_delete=models.CASCADE)
    amount = models.FloatField()

    def __str__(self):
        return self.ingredient

    def get_amount(self):
        if self.amount == int(self.amount):
            return int(self.amount)
        else:
            return self.amount


class AmountUnit(models.Model):
    name = models.CharField(max_length=36)
    unit_of = models.CharField(max_length=1,
                               choices=[('V', 'Volume'),
                                        ('W', 'Weight')],
                               default='V')
    system = models.CharField(max_length=1,
                               choices=[('M', 'Metric'),
                                        ('I', 'Imperial')],
                               default='M')

    def __str__(self):
        return self.name


class Manufacturer(models.Model):
    name = models.CharField(max_length=99)
    website = models.URLField()
    description = models.TextField(blank=True, null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class Step(OrderedModel):
    # For class Meta use: class Meta(OrderedModel.Meta)
    category = models.ForeignKey('StepCategory', null=True, on_delete=models.SET_NULL)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    duration = models.DurationField()
    description = HTMLField('Description', blank=True, null=True)

    order_with_respect_to = 'recipe'

    def __str__(self):
        return '%i %s in %s' % (self.order + 1, self.category.name, self.recipe.name)

    def _add_time(self, value, append):
        if self.time_output and value:
            self.time_output += ', {} {}'.format(value, append)
        elif value:
            self.time_output = '{} {}'.format(value, append)

    def format_duration(self):
        self.time_output = None
        total_minutes = self.duration.seconds // 60
        self._add_time(self.duration.days, 'days')
        self._add_time((total_minutes // 60), 'hours')
        self._add_time((total_minutes % 60), 'minutes')
        return self.time_output


class StepCategory(models.Model):
    name = models.CharField(max_length=99)
    category = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
