from django.forms.fields import DurationField
import datetime


class IntDurationField(DurationField):
    def to_python(self, value):
        if value in self.empty_values:
            return None
        if isinstance(value, datetime.timedelta):
            return value
        return int(value)
