from django.db import models
from django.conf import settings

from ..recipe.models import Step

class Log(models.Model):
    batch_number = models.IntegerField()
    original_gravity = models.FloatField(blank=True, null=True)
    final_gravity = models.FloatField(blank=True, null=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['-pub_date']

    def __str__(self):
        return self.name


class Note(models.Model):
    step = models.ForeignKey(Step, on_delete=models.CASCADE)
    log = models.ForeignKey(Log, on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    description = models.TextField()
