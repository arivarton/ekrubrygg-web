from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),

    path('<int:recipe_id>/', views.detail, name='detail'),
    path('<int:recipe_id>/edit/', views.edit, name='edit'),

    path('my_recipes/', views.created_recipes, name='created_recipes'),
    path('my_ingredients/', views.created_ingredients, name='created_ingredients'),
    path('my_ingredients/<int:ingredient_id>/', views.edit_ingredient, name='edit_ingredient'),
    path('add/', views.add, name='add'),

    path('<int:recipe_id>/delete/', views.delete, name='delete'),
    path('<int:recipe_id>/delete/step/<int:step_id>/', views.delete_step, name='delete_step'),
    path('<int:recipe_id>/delete/ingredient/<int:ingredient_id>/', views.delete_ingredient_from_recipe, name='delete_ingredient_from_recipe'),
    path('<int:recipe_id>/delete/step/ingredient/<int:step_ingredient_id>/', views.delete_ingredient_from_step, name='delete_ingredient_from_step'),
    path('<int:recipe_id>/move/<int:step_id>/', views.move_step, name='move_step'),
    path('<int:recipe_id>/publicize/', views.publicize, name='publicize'),
    path('<int:recipe_id>/add_step/', views.add_step, name='add_step'),
    path('<int:recipe_id>/ingredient/<str:action>', views.add_ingredient_to_recipe, name='add_ingredient_to_recipe'),
    path('<int:recipe_id>/edit/ingredient/<int:ingredient_id>/', views.edit_ingredient_in_recipe, name='edit_ingredient_in_recipe'),
    path('<int:recipe_id>/edit/step/<int:step_id>/', views.edit_step_description, name='edit_step_description'),

    path('ingredient/<str:action>', views.add_ingredient, name='add_ingredient'),

    path('<str:brew_type>/', views.brew_filter, name='brew_filter'),
]
