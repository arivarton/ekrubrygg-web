from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form = form.save()
            messages.success(request, 'Succesfully registered user!')
            return redirect('account:profile')
    else:
        form = UserCreationForm()
    context = {'form': form}
    return render(request, 'account/register.html', context)

@login_required
def profile(request):
    return render(request, 'account/profile.html')
