from django.contrib import admin

from .models import BrewCategory, AmountUnit, IngredientCategory, StepCategory

# Register your models here.
admin.site.register(BrewCategory)
admin.site.register(AmountUnit)
admin.site.register(IngredientCategory)
admin.site.register(StepCategory)
