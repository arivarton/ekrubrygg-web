$( document ).ready(function() {
    $('select').on('change', function(event) {
      $(this).parent().parent().find("#amount_unit").text($( "option:selected", this).attr("data-amount_unit"));
      id_amount = $(this).parent().parent().find("#id_amount");
      data_amount = $( "option:selected", this).attr("data-amount");
      id_amount.attr("max", data_amount);
      id_amount.val(data_amount);
    });
});
