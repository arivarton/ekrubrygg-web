from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.forms import formset_factory

from ..recipe.models import Recipe, Ingredient, IngredientInRecipe, Step, Manufacturer, IngredientInStep
from ..recipe.forms import RecipeForm, IngredientForm, IngredientInRecipeForm, IngredientInStepForm, StepForm, StepDescriptionForm

def index(request):
    title = 'Recipes'
    recipes = Recipe.objects.filter(public=True)
    context = {
        'recipes': recipes,
        'title': title
    }
    return render(request, 'recipe/index.html', context)

def detail(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    if not recipe.public and recipe.created_by != request.user:
        messages.error(request, 'This recipe is not publicized!')
        return redirect('recipe:index')
    steps = Step.objects.filter(recipe=recipe).order_by('order')
    ingredients = IngredientInRecipe.objects.filter(recipe=recipe)
    context = {'recipe': recipe,
               'steps': steps,
               'ingredients': ingredients}
    return render(request, 'recipe/detail.html', context)

@login_required
def edit(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id, created_by=request.user)
    if recipe.public:
        messages.error(request, 'Unpublicize recipe before editing!')
        return redirect('recipe:detail', recipe.id)
    else:
        if request.method == 'POST':
            form = IngredientInStepForm(recipe, request.POST)
            if form.is_valid():
                form = form.save(commit=False)
                form.step = get_object_or_404(Step, pk=request.POST['step'])
                if not form.amount:
                    messages.error(request, 'Specify a value higher than zero!')
                else:
                    try:
                        existing_ingredient = form.step.ingredientinstep_set.get(ingredient__ingredient=form.ingredient.ingredient)
                        existing_ingredient.amount += form.amount
                        existing_ingredient.save()
                        messages.success(request, 'Succesfully increased ingredient!')
                    except IngredientInStep.DoesNotExist:
                        form = form.save()
                        messages.success(request, 'Succesfully added ingredient to step!')
                return redirect('recipe:edit', recipe_id)
        else:
            ingredients = IngredientInRecipe.objects.filter(recipe=recipe)
            steps = Step.objects.filter(recipe=recipe).order_by('order')
            StepDescriptionFormset = formset_factory(StepDescriptionForm)
            formset = StepDescriptionFormset(initial=[{'description': step.description} for step in steps])
            steps_and_forms = [{'step': step, 'form': formset[index]} for index, step in enumerate(steps)]
            context = {'recipe': recipe,
                       'steps': steps,
                       'ingredients': ingredients,
                       'step_description_form': formset,
                       'steps_and_forms': steps_and_forms}
            return render(request, 'recipe/edit_recipe.html', context)

@login_required
def created_recipes(request):
    title = 'My recipes'
    recipes = Recipe.objects.filter(created_by=request.user)
    context = {'recipes': recipes, 'title': title}
    return render(request, 'recipe/created_recipes.html', context)

@login_required
def created_ingredients(request):
    title = 'My ingredients'
    ingredients = Ingredient.objects.filter(created_by=request.user)
    context = {'ingredients': ingredients, 'title': title}
    return render(request, 'recipe/created_ingredients.html', context)

@login_required
def add(request):
    if request.method == 'POST':
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.instance.created_by = request.user
            form = form.save()
            messages.success(request, 'Succesfully added recipe!')
            return redirect('recipe:detail', form.id)
    else:
        form = RecipeForm()
    context = {'form': form}
    return render(request, 'recipe/add_form.html', context)

@login_required
def delete(request, recipe_id):
    if request.method == 'POST':
        recipe = get_object_or_404(Recipe, pk=recipe_id, created_by=request.user)
        recipe.delete()
        messages.success(request, 'Succesfully removed recipe!')
        return redirect('recipe:created')
    else:
        return redirect('recipe:detail', recipe_id)


@login_required
def add_step(request, recipe_id):
    title = 'Add step'
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    if recipe.created_by != request.user:
        return render(request, '403.html', status=403)
    else:
        if request.method == 'POST':
            step_form = StepForm(request.POST)
            if step_form.is_valid():
                step_form = step_form.save(commit=False)
                step_form.recipe_id = recipe_id
                step_form.save()
                return redirect('recipe:edit', recipe_id)
        else:
            step_form = StepForm()
        context = {'form': step_form, 'title': title}
    return render(request, 'recipe/add_form.html', context)

@login_required
def delete_step(request, recipe_id, step_id):
    if request.method == 'POST':
        step = get_object_or_404(Step, pk=step_id, recipe__created_by=request.user)
        step.delete()
        messages.success(request, 'Succesfully removed step!')
    return redirect('recipe:edit', recipe_id)

@login_required
def edit_step_description(request, recipe_id, step_id):
    if request.method == 'POST':
        step = Step.objects.get(id=step_id)
        request.POST._mutable = True
        request.POST['description'] = request.POST.popitem()[1][0]
        request.POST._mutable = False
        form = StepDescriptionForm(request.POST, initial={'description': step.description}, instance=step)
        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.success(request, 'Succesfully edited step description!')
            else:
                messages.info(request, 'No change detected in step description!')
    return redirect('recipe:edit', recipe_id)

@login_required
def delete_ingredient_from_step(request, recipe_id, step_ingredient_id):
    if request.method == 'POST':
        ingredient = get_object_or_404(IngredientInStep, pk=step_ingredient_id, ingredient__ingredient__created_by=request.user)
        ingredient.delete()
        messages.success(request, 'Succesfully removed ingredient from step!')
    return redirect('recipe:edit', recipe_id)

@login_required
def move_step(request, recipe_id, step_id):
    if request.method == 'POST':
        step = get_object_or_404(Step, pk=step_id, recipe__created_by=request.user)
        if request.POST['action'] == 'up':
            step.up()
        elif request.POST['action'] == 'down':
            step.down()
        elif request.POST['action'] == 'arbitrary':
            step.to(int(request.POST['move_to']))
        messages.success(request, 'Succesfully moved step %s!' % request.POST['action'])
    return redirect('recipe:detail', recipe_id)

@login_required
def publicize(request, recipe_id):
    if request.method == 'POST':
        recipe = get_object_or_404(Recipe, pk=recipe_id, created_by=request.user)
        if not recipe.public:
            for ingredient in recipe.ingredientinrecipe_set.all():
                if ingredient.amount_left():
                    messages.error(request, 'Every ingredient must be used completely in the recipe before publishing!')
                    return redirect('recipe:detail', recipe_id)
            message = 'Succesfully publicized recipe!'
        else:
            message = 'Succesfully unpublicized recipe!'
        recipe.public = not recipe.public
        recipe.save()
        messages.success(request, message)
    return redirect('recipe:detail', recipe_id)


@login_required
def add_ingredient(request, action):
    title = 'Add ingredient'
    if request.method == 'POST':
        try:
            instance = Ingredient.objects.get(name=request.POST['name'],
                                              category=request.POST['category'])
            ingredient_form = IngredientForm(instance=instance,
                                             data=request.POST)
        except Ingredient.DoesNotExist:
            ingredient_form = IngredientForm(request.POST)
        if ingredient_form.is_valid():
            ingredient_form = ingredient_form.save(commit=False)
            ingredient_form.created_by = request.user
            ingredient_form.manufacturer.created_by = request.user
            ingredient_form.save()
            messages.success(request, 'Succesfully added ingredient!')
            if action == 'more':
                return redirect('recipe:add_ingredient', 'add')
            else:
                return redirect('recipe:created_ingredients')
    else:
        ingredient_form = IngredientForm()
    available_ingredients = Ingredient.objects.filter(created_by=request.user)
    available_manufacturers = Ingredient.objects.filter(created_by=request.user)
    context = {'form': ingredient_form,
               'available_ingredients': available_ingredients,
               'available_manufacturers': available_manufacturers,
               'title': title}
    return render(request, 'recipe/add_ingredient_form.html', context)

@login_required
def edit_ingredient(request, ingredient_id):
    ingredient = get_object_or_404(Ingredient, pk=ingredient_id, created_by=request.user)
    title = 'Edit %s' % ingredient.name.lower()
    data = {'manufacturer': ingredient.manufacturer.name,
            'manufacturer_website': ingredient.manufacturer.website}
    if request.method == 'POST':
        form = IngredientForm(request.POST, initial=data, instance=ingredient, user=request.user)
        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.success(request, 'Succesfully edited ingredient!')
                return redirect('recipe:created_ingredients')
            else: 
                messages.info(request, 'No change detected!')
                return redirect('recipe:created_ingredients')
    else:
        form = IngredientForm(initial=data, instance=ingredient)
        available_ingredients = Ingredient.objects.filter(created_by=request.user)
        available_manufacturers = Manufacturer.objects.filter(created_by=request.user)
        context = {'form': form,
                   'available_ingredients': available_ingredients,
                   'available_manufacturers': available_manufacturers,
                   'title': title}
        return render(request, 'recipe/edit_ingredient_form.html', context)


@login_required
def add_ingredient_to_recipe(request, recipe_id, action):
    title = 'Add ingredient to recipe'
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    if recipe.created_by != request.user:
        return render(request, '403.html', status=403)
    else:
        if request.method == 'POST':
            form = IngredientInRecipeForm(request.POST, user=request.user)
            if form.is_valid():
                ingredient = form.cleaned_data['ingredient']
                amount = form.cleaned_data['amount']
                try: 
                    existing_ingredientinrecipe = IngredientInRecipe.objects.get(ingredient=ingredient, recipe=recipe)
                    existing_ingredientinrecipe.amount += amount
                    existing_ingredientinrecipe.save()
                except IngredientInRecipe.DoesNotExist:
                    form = form.save(commit=False)
                    form.ingredient_id = ingredient.id
                    form.recipe_id = recipe_id
                    form.save()
                messages.success(request, 'Succesfully added ingredient to recipe!')
                if action == 'more':
                    return redirect('recipe:add_ingredient_to_recipe', recipe_id, 'add')
                else:
                    return redirect('recipe:edit', recipe_id)
        else:
            form = IngredientInRecipeForm()
        available_ingredients = Ingredient.objects.filter(created_by=request.user)
        available_manufacturers = Manufacturer.objects.filter(created_by=request.user)
        context = {'form': form,
                   'available_ingredients': available_ingredients,
                   'available_manufacturers': available_manufacturers,
                   'recipe_id': recipe_id,
                   'title': title}
        return render(request, 'recipe/add_ingredient_to_recipe_form.html', context)

@login_required
def edit_ingredient_in_recipe(request, recipe_id, ingredient_id):
    ingredient = get_object_or_404(IngredientInRecipe, pk=ingredient_id, ingredient__created_by=request.user)
    title = 'Edit %s' % ingredient.ingredient.name.lower()
    data = {'name': ingredient.ingredient.name,
            'manufacturer': ingredient.ingredient.manufacturer.name,
            'manufacturer_website': ingredient.ingredient.manufacturer.website,
            'description': ingredient.ingredient.description,
            'category': ingredient.ingredient.category}
    if request.method == 'POST':
        form = IngredientInRecipeForm(request.POST, initial=data, instance=ingredient, user=request.user)
        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.success(request, 'Succesfully edited ingredient!')
                return redirect('recipe:edit', recipe_id)
            else: 
                messages.info(request, 'No change detected!')
                return redirect('recipe:edit', recipe_id)
    else:
        form = IngredientInRecipeForm(initial=data, instance=ingredient)
        available_ingredients = Ingredient.objects.filter(created_by=request.user)
        available_manufacturers = Manufacturer.objects.filter(created_by=request.user)
        context = {'form': form,
                   'available_ingredients': available_ingredients,
                   'available_manufacturers': available_manufacturers,
                   'title': title}
        return render(request, 'recipe/edit_form.html', context)

@login_required
def delete_ingredient_from_recipe(request, recipe_id, ingredient_id):
    if request.method == 'POST':
        ingredient = get_object_or_404(IngredientInRecipe, pk=ingredient_id, ingredient__created_by=request.user)
        ingredient.delete()
        messages.success(request, 'Succesfully removed ingredient from recipe!')
    return redirect('recipe:edit', recipe_id)

def brew_filter(request, brew_type):
    pass
