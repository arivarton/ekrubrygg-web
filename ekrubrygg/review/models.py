from django.db import models
from django.conf import settings

from ..log.models import Log
from ..recipe.models import Recipe


class Review(models.Model):
    # 0 to 10 in rating
    RATING_CHOICES = [(i, i) for i in range(0, 11)]

    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    log = models.ForeignKey(Log, blank=True, null=True, on_delete=models.SET_NULL)
    pub_date = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(choices=RATING_CHOICES)
    comment = models.TextField()
    posted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['-pub_date']

    def __str__(self):
        return 'Review for %s by %s' % (self.recipe.name, self.posted_by.username)
