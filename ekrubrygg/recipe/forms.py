from datetime import timedelta

from django.forms import ModelForm, NumberInput, URLField, CharField, ModelChoiceField, ChoiceField, IntegerField, Textarea
from django.core.exceptions import ValidationError

from .models import Recipe, Ingredient, IngredientInRecipe, IngredientInStep, IngredientCategory, Manufacturer, StepCategory, Step
from .fields import IntDurationField


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        exclude = ('public', 'ingredients', 'pub_date', 'created_by')
        widgets = {
            'original_gravity': NumberInput(attrs={'step': '0.001',
                                                   'value': '1.099'}),
            'final_gravity': NumberInput(attrs={'step': '0.001',
                                                'value': '1.000'})}


class StepForm(ModelForm):
    duration = IntegerField()
    duration_type = ChoiceField(choices=[('M', 'Minutes'),
                                         ('H', 'Hours'),
                                         ('D', 'Days')])

    class Meta:
        model = Step
        exclude = ('recipe',)

    def clean(self):
        cleaned_data = super().clean()
        duration = cleaned_data.get('duration')
        duration_type = cleaned_data.get('duration_type')
        if duration_type == 'M':
            cleaned_data['duration'] = timedelta(minutes=duration)
        elif duration_type == 'H':
            cleaned_data['duration'] = timedelta(hours=duration)
        elif duration_type == 'D':
            cleaned_data['duration'] = timedelta(days=duration)
        else:
            raise ValidationError('Invalid duration type!', code='invalid')
        return cleaned_data


class IngredientForm(ModelForm):
    manufacturer = CharField(max_length=99, min_length=2, strip=True)
    manufacturer_website = URLField(required=False, help_text='Not required')

    class Meta:
        model = Ingredient
        exclude = ('created_by',)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(IngredientForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        
        manufacturer = Manufacturer.objects.get_or_create(
            name=self.cleaned_data['manufacturer'],
            created_by=self.user
        )
        if cleaned_data['manufacturer_website']:
            manufacturer[0].website = cleaned_data['manufacturer_website']

        cleaned_data['manufacturer'] = manufacturer[0]

        print('\n\n')
        print('%s\n%s' % (manufacturer[1], self.instance.manufacturer.ingredient.all()))
        print('\n\n')
        # Delete manufacturer if it doesn't belong to any ingredients after change of model
        if hasattr(self.instance, 'manufacturer') and self.instance.manufacturer.ingredient.all().count() == 1:
            print('\n\n')
            print('Is it going inside if?')
            print('\n\n')
            self.instance.manufacturer.delete()

        return cleaned_data


class IngredientInRecipeForm(ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    name = CharField(max_length=99, min_length=2, strip=True)
    manufacturer = CharField(max_length=99, min_length=2, strip=True)
    manufacturer_website = URLField(required=False)
    description = CharField(min_length=2, strip=True, widget=Textarea(), required=False)
    category = ModelChoiceField(queryset=IngredientCategory.objects.all())

    field_order = ['name']

    class Meta:
        model = IngredientInRecipe
        exclude = ('recipe', 'ingredient', 'steps')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(IngredientInRecipeForm, self).__init__(*args, **kwargs)
        self.fields['amount_unit'].empty_label = None
        self.fields['amount_unit'].label = ''

    def clean(self):
        cleaned_data = super().clean()
        manufacturer = Manufacturer.objects.get_or_create(
            name=cleaned_data['manufacturer'],
            created_by=self.user
        )
        if cleaned_data['manufacturer_website']:
            manufacturer[0].website = cleaned_data['manufacturer_website']

        # Delete manufacturer if it doesn't belong to any ingredients after change of model
        if hasattr(self.instance, 'ingredient') and self.instance.ingredient.manufacturer.ingredient_set.all().count() == 1:
            self.instance.ingredient.manufacturer.delete()

        ingredient = Ingredient.objects.get_or_create(
            name=cleaned_data['name'],
            created_by=self.user,
            manufacturer=manufacturer[0],
            category=cleaned_data['category']
        )

        # Delete ingredient if it doesn't belong to any recipe after change of model
        if hasattr(self.instance, 'ingredient') and self.instance.ingredient.ingredientinrecipe_set.all().count() == 1:
            self.instance.ingredient.delete()

        cleaned_data['ingredient'] = ingredient[0]
        self.instance.ingredient = ingredient[0]

        return cleaned_data


class IngredientSelectField(ModelChoiceField):
    def label_from_instance(self, obj):
        return "%s by %s (%s)" % (obj.ingredient.name, obj.ingredient.manufacturer.name, obj.amount_unit)


class IngredientInStepForm(ModelForm):
    ingredient = IngredientSelectField(queryset=IngredientInRecipe.objects.all())

    class Meta:
        model = IngredientInStep
        exclude = ('step',)

    def __init__(self, recipe, *args, **kwargs):
        super(IngredientInStepForm, self).__init__(*args, **kwargs)
        self.fields['ingredient'].queryset = self.fields['ingredient'].queryset.filter(recipe=recipe)
        self.fields['ingredient'].empty_label = 'Choose ingredient'


class StepDescriptionForm(ModelForm):
    class Meta:
        model = Step
        fields = ('description',)
