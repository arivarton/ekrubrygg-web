from django.apps import AppConfig


class RecipeConfig(AppConfig):
    name = 'ekrubrygg.recipe'
