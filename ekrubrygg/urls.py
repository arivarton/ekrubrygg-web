"""ekrubrygg URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from .core.urls import urlpatterns as core_urls
from .recipe.urls import urlpatterns as recipe_urls
from .account.urls import urlpatterns as account_urls

from .account.views import profile as profile_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(core_urls)),
    path('recipe/', include((recipe_urls, 'recipe'), namespace='recipe')),
    path('accounts/profile/', profile_view, name='profile'),
    path('accounts/', include('allauth.urls')),
    path('tinymce/', include('tinymce.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
